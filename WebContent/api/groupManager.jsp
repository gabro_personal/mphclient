<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gestione Gruppi</title>
<script src="../jquery/jquery-1.7.1.js"></script>
<script language="Javascript">
function nuovoGruppo() {
	document.getElementById('existGroup').style.display = "none";
	document.getElementById('newGroup').style.display = "block";
	}
function aggiornaPrj() {
	document.getElementById('newGroupName').value="";
	document.getElementById('existGroup').style.display = "block";
	document.getElementById('newGroup').style.display = "none";
	$.get("getDynamicList?listType=1&prjId="+document.getElementById('prjId').value, function(response) { document.getElementById('existGroup').innerHTML=response; });
}
</script>
</head>
<body onload="aggiornaPrj();">
<%@ include file="header.jsp" %>
<%
List<ProjectDetails> prjDet=(List<ProjectDetails>)request.getAttribute( "prjList" );
List<GroupDetails> myGroup=(List<GroupDetails>)request.getAttribute( "myGroupList" );
%>

<div id="stylized" class="myform" <% if(myGroup.size()==0) out.print("style=\"display: none\""); %>>
<form id="setGroup_form" name="setGroup_form" action="doSetGroup" method="post" >
<h1>Gestisci Gruppi</h1>
<p>Permette di selezionare un Gruppo per il Contesto Corrente o di Uscire da un Gruppo</p>

<label>Gruppo
<span class="small">Gruppi a cui sei Iscritto</span>
</label>
<select id="setGroupId" name="setGroupId">
	<%
	for(int i=0;i<myGroup.size();i++)
		out.print("<option value='"+myGroup.get(i).getId()+"'>"+myGroup.get(i).getName()+"</option>");
	%>
</select>

<button type="submit" name="action" value="Seleziona Gruppo">Seleziona</button>
<div class="spacer"></div>
<button type="submit" name="action" value="Rimuovi da Gruppo">Esci</button>
<div class="spacer"></div>

</form>
</div>

<br>

<div id="stylized" class="myform" <% if(myGroup.size()==3) out.print("style=\"display: none\""); %>>
<form id="addGroup_form" name="addGroup_form" action="doAddGroup" method="post" >
<h1>Aggiungi Gruppo</h1>
<p>Permette di Iscriversi ad un Gruppo o di Crearlo</p>

<label>Progetto
<span class="small">Progetti Attivi</span>
</label>
<select id="prjId" name="prjId" onChange="aggiornaPrj();">
	<%
	for(int i=0;i<prjDet.size();i++)
		out.print("<option value='"+prjDet.get(i).getId()+"'>"+prjDet.get(i).getName()+"</option>");
	%>
</select>

<div id="existGroup" style="display: block"></div>
<div id="newGroup" style="display: none">

<label>Nuovo Gruppo
<span class="small">Nome del Nuovo Gruppo</span>
</label>
<input type="text" id="newGroupName" name="newGroupName" /><br>
</div>
<button type="submit" name="action" value="Aggiungi Gruppo">Entra</button>
<div class="spacer"></div>
</form>
</div>
<%@ include file="footer.jsp" %>
</body>
</html>