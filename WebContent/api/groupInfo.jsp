<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informazioni Gruppo</title>
</head>
<body>
<%
List<StudentDetails> listaComponenti=(List<StudentDetails>)request.getAttribute( "listaComponenti" );
ProjectDetails progettoAssegnato=(ProjectDetails)request.getAttribute( "progettoAssegnato" );
List<DeliverableDetails> listaDeliverable=(List<DeliverableDetails>)request.getAttribute( "listaDeliverable" );
GroupDetails groupDet=(GroupDetails)request.getAttribute( "groupDet" );
HashMap<Integer,DocumentDetails> docMap=(HashMap<Integer,DocumentDetails>)request.getAttribute( "docMap" );
%>
<%@ include file="header.jsp" %>
<b>Gruppo:</b> <% if(groupDet!=null) out.print(groupDet.getName());%><br>
<b>Nome Progetto Assegnato:</b> <a href="prjInfo"><% if(progettoAssegnato!=null) out.print(progettoAssegnato.getName());%></a><br>
<% if(groupDet!=null) 
		if(groupDet.getFinalScore()!=0) 
			out.print("<b>Voto Finale: </b>"+groupDet.getFinalScore()+"<br>");
		%>
<br><b>Lista Componenti:</b>
<table>
<thead>
   <tr>
   	  <th>Avatar</th>
      <th>Nome</th>
      <th>Cognome</th>
      <th>Matricola</th>
    </tr>
</thead>
<tbody>
<%
if(listaComponenti!=null)
	for(int i=0;i<listaComponenti.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print("<img width=25px height=25px src=\"getFile?type=img&id="+listaComponenti.get(i).getId()+"\"></img>");
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getFirstName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getLastName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getMatrNo());
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>
<br><b>Lista Deliverable:<b>
<table>
<thead>
   <tr>
      <th>Nome</th>
      <th>Descrizione</th>
      <th>Deadline</th>
      <th>Consegna</th>
      <th>Voto</th>
    </tr>
</thead>
<tbody>
<%
if(listaDeliverable!=null)
	for(int i=0;i<listaDeliverable.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getDescription());
		out.print("</td>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getDeadline().getTime());
		out.print("</td>");
		out.print("<td>");
		DocumentDetails localDoc=docMap.get(listaDeliverable.get(i).getId());
		if(localDoc==null){	
			out.print("<form id=\"docAdd_form\" name=\"docAdd_form\" action=\"doAddDocument\" method=\"post\" enctype=\"multipart/form-data\">");
			out.print("<input type=\"file\" name=\"document\" accept=\"application/pdf\"/>");
			out.print("<input type=\"hidden\" name=\"action\" value=\"new\"/>");
			out.print("<input type=\"hidden\" name=\"deliverableId\" value=\""+ listaDeliverable.get(i).getId() +"\"/>");
			out.print("<input type=\"hidden\" name=\"groupId\" value=\""+ groupDet.getId() +"\"/>");
			out.print("<input type=\"submit\" value=\"Upload\"/></form>");		
		}else{
			out.print(localDoc.getDeliveryDate().getTime()+" - ");
			if(localDoc.getScore()==0){	
			out.print("<a href=\"getFile?type=pdf&id="+localDoc.getId()+"\">Download</a><br><br>");
			out.print("<form id=\"docAdd_form\" name=\"docAdd_form\" action=\"doAddDocument\" method=\"post\" enctype=\"multipart/form-data\">");
			out.print("<input type=\"file\" name=\"document\" accept=\"application/pdf\"/>");
			out.print("<input type=\"hidden\" name=\"action\" value=\"update\"/>");
			out.print("<input type=\"hidden\" name=\"documentId\" value=\""+ localDoc.getId() +"\"/>");
			out.print("<input type=\"submit\" value=\"Upload\"/></form>");						
		}
			}
		out.print("</td>");
		out.print("<td>");

		if(localDoc!=null)
			if(localDoc.getScore()!=0)
			 out.print(localDoc.getFinalScore());
			else
			 out.print("Non Valutato");
		else
			 out.print("Non Consegnato");
		
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>
<%@ include file="footer.jsp" %>
</body>
</html>