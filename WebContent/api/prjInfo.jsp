<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informazioni Progetto</title>
</head>
<body>
<%@ include file="header.jsp" %>
<%
ProjectDetails prjDet=(ProjectDetails)request.getAttribute( "prjDet" );
Integer nIscritti=(Integer)request.getAttribute( "prjGNum" );
ProfessorDetails profName=(ProfessorDetails)request.getAttribute( "profName" );
%>
<b>Progetto: </b><% if(prjDet!=null) out.print(prjDet.getName());%><br>
<p align="justify"><b>Descrizione: </b><% if(prjDet!=null) out.print(prjDet.getDescription()); %></p><br>
<b>Assengato da: </b> <% if(profName!=null) out.print(profName.getFirstName()+" "+profName.getLastName());%><br>
<b>Iscritti: </b> <%if(nIscritti!=null) out.print(nIscritti);%> gruppo/i<br>
<%@ include file="footer.jsp" %>
</body>
</html>