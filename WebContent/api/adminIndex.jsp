<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin!</title>
</head>
<body>
<%@ include file="headerAdmin.jsp" %>
<%
List<ProfessorDetails> profList=(List<ProfessorDetails>)request.getAttribute( "profList" );
%>
<b>Lista Professori:</b> 
<table>
<thead>
   <tr>
      <th>Nome</th>
      <th>Cognome</th>
      <th>Username</th>
    </tr>
</thead>
<tbody>
<%
if(profList!=null)
	for(int i=0;i<profList.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print(profList.get(i).getFirstName());
		out.print("</td>");
		out.print("<td>");
		out.print(profList.get(i).getLastName());
		out.print("</td>");
		out.print("<td>");
		out.print(profList.get(i).getUsername());
		out.print("</td>");
		out.print("<td>");
		out.print("<div id=\"stylized\" class=\"myform\">");
		out.print("<form id=\"editProf_form\" name=\"editProf_form\" action=\"doEditProf\" method=\"post\">");
		out.print("<label>Nome</label><input type=\"text\" name=\"firstName\" /><br>");
		out.print("<label>Cognome</label><input type=\"text\" name=\"lastName\" /><br>");
		out.print("<label>Username</label><input type=\"text\" name=\"username\" /><br>");
		out.print("<label>Password</label><input type=\"password\" name=\"password\" /><br>");
		out.print("<input type=\"hidden\" name=\"profId\" value=\""+profList.get(i).getId()+ "\">");
		out.print("<button type=\"submit\" value=\"Modifica\">Modifica</button>");
		out.print("<div class=\"spacer\"></div>");
		out.print("</form>");
		out.print("</div>");
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>
<br>

<div id="stylized" class="myform">
<form id="newProf_form" name="newProf_form" action="doRegisterProf" method="post">
<h1>Aggiungi Professore</h1>
<p>Permette di Creare un Nuovo Professore</p>
	<label>Nome</label><input type="text" name="firstName" /><br>
	<label>Cognome</label><input type="text" name="lastName" /><br>
	<label>Username</label><input type="text" name="username" /><br>
	<label>Password</label><input type="password" name="password" /><br>
<button type="submit" value="Crea">Crea</button>
<div class="spacer"></div>
</form>
</div>



<%@ include file="footer.jsp" %>
</body>
</html>