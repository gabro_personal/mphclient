<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informazioni Gruppo</title>
</head>
<body>
<%@ include file="headerProf.jsp" %>
<%
List<StudentDetails> listaComponenti=(List<StudentDetails>)request.getAttribute( "listaComponenti" );
ProjectDetails progettoAssegnato=(ProjectDetails)request.getAttribute( "progettoAssegnato" );
GroupDetails groupDet=(GroupDetails)request.getAttribute( "groupDet" );
%>
<b>Gruppo:</b> <% if(groupDet!=null) out.print(groupDet.getName());%><br>
<b>Nome Progetto Assegnato:</b> <a href="prjInfo"><% if(progettoAssegnato!=null) out.print(progettoAssegnato.getName());%></a><br>
<br><b>Lista Componenti:</b> 
<table>
<thead>
   <tr>
   	  <th>Avatar</th>
      <th>Nome</th>
      <th>Cognome</th>
      <th>Matricola</th>
    </tr>
</thead>
<tbody>
<%
if(listaComponenti!=null)
	for(int i=0;i<listaComponenti.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print("<img width=25px height=25px src=\"getFile?type=img&id="+listaComponenti.get(i).getId()+"\"></img>");
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getFirstName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getLastName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaComponenti.get(i).getMatrNo());
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>
<%@ include file="footer.jsp" %>
</body>
</html>