<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<%
ProjectDetails prjDet=(ProjectDetails)request.getAttribute( "prjDet" );
List<DeliverableDetails> listaDeliverable=(List<DeliverableDetails>)request.getAttribute( "listaDeliverable" );
Map<Integer,GroupDetails> mapGruppi=(Map<Integer,GroupDetails>)request.getAttribute("mapGroup");
Map<Integer,List<DocumentDetails>> mapDocumenti=(Map<Integer,List<DocumentDetails>>)request.getAttribute("mapDocument");
%>
<body>
<table>
<thead>
   <tr>
      <th>Nome</th>
    </tr>
</thead>
<tbody>
<%
if(listaDeliverable!=null)
	for(int i=0;i<listaDeliverable.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getName());
		out.print("</td>");
		List<DocumentDetails> docList=mapDocumenti.get(listaDeliverable.get(i).getId());
				if(docList!=null)
					for(int j=0;j<docList.size();j++){
						int currDocId=docList.get(j).getId();
						GroupDetails currGroup=mapGruppi.get(currDocId);
						out.print("<td>");
						out.print("<a href=\"groupInfoProf?id="+currGroup.getId()+"\">"+currGroup.getName()+"</a>");	
						out.print(" - ");	
						out.print("<a href=\"getFile?type=pdf&id="+currDocId+"\"> Download</a>");
						out.print("</td>");
						out.print("<td>");
						out.print("<div id=\"stylized\" class=\"myform\">");
						out.print("<form id=\"valuta_form_"+j+"\" name=\"valuta_form\" action=\"doValuta\" method=\"post\">");
						if(docList.get(j).getScore()==0) 
						out.print("<label>Voto</label><input type=\"text\" name=\"voto\" value=\""+"Non Valutato"+"\" />");
						else
						out.print("<label>Voto</label><input type=\"text\" name=\"voto\" value=\""+docList.get(j).getScore()+"\" />");
						out.print("<input type=\"hidden\" name=\"docId\" value=\""+currDocId+"\" />");
						out.print("<button type=\"submit\" value=\"Valuta\">Valuta</button>");
						out.print("</form>");
						out.print("</div>");
						out.print("</td>");
					}
		out.print("<td>");
		out.print("<a href=\"getFile?type=zip&id="+listaDeliverable.get(i).getId()+"&subType=byDeliv\">Download By Deliverable</a>");
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>






<%

%>


</body>
</html>