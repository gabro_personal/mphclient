<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="mph.remote.*"%>
<%@page import="mph.client.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%@ include file="error.jsp" %>
<%
StudentManagerRemote studentManager= (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
StudentDetails studDetailA=null;
try{
studDetailA = studentManager.getStudent(mphCk, mphCk.getUserID());
} catch (Exception e) {
request.getRequestDispatcher("login").forward(request, response);
}
%>
<table>
<tr>
<td>
<% if(studDetailA!=null) out.print("<img width=75px height=75px src=\"getFile?type=img&id="+studDetailA.getId()+"\"></img>");%><br>
</td><td>
<% if(studDetailA!=null) out.print(studDetailA.getFirstName());%>
<% if(studDetailA!=null) out.print(studDetailA.getLastName());%>
 - <% if(studDetailA!=null) out.print(studDetailA.getMatrNo());%><br>
<a href="doLogout"><span>Logout</a><br>
<td>
</tr>
</table>
<ul class="menu">
 <li><a href="index"><span>Index</a></li>
 <li><a href="groupManager"><span>Gestisci Gruppi</span></a></li>
 <li><a href="prjInfo"><span>Info Progetto</span></a></li>
 <li><a href="groupInfo"><span>Info Gruppo</span></a></li>
 <li><a href="editStudent"><span>Modifica Dati Personali</span></a></li>
</ul>
<br>
</body>
</html>