<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="mph.client.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<%
ProjectDetails prjDet=(ProjectDetails)request.getAttribute( "prjDet" );
List<DeliverableDetails> listaDeliverable=(List<DeliverableDetails>)request.getAttribute( "listaDeliverable" );
%>
<body>

<div id="stylized" class="myform">
<form id="editPrj_form" name="editPrj_form" action="doEditProject" method="post">
<h1>Modifica Progetto</h1>
<p>Permette di Modificare un Progetto</p>
	<label>Nome<span class="small"></span></label><input type="text" name="name" value="<% if(prjDet!=null) out.print(prjDet.getName());%>"/><br>
	<label>Descrizione<span class="small"></span></label><textarea name="description"><% if(prjDet!=null) out.print(prjDet.getDescription()); %></textarea><br>
	<input type="hidden" name="prjId" value="<% if(prjDet!=null) out.print(prjDet.getId()); %>">
<button type="submit" name="action" value="Modifica">Modifica</button>
<div class="spacer"></div>
</form>
</div>


<br>
<b>Lista Deliverable:</b>
<table>
<thead>
   <tr>
      <th>Nome</th>
      <th>Descrizione</th>
      <th>Deadline</th>
    </tr>
</thead>
<tbody>
<%
if(listaDeliverable!=null)
	for(int i=0;i<listaDeliverable.size();i++){
		out.print("<tr>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getName());
		out.print("</td>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getDescription());
		out.print("</td>");
		out.print("<td>");
		out.print(listaDeliverable.get(i).getDeadline().getTime());
		out.print("</td>");
		out.print("<td>");
		out.print("<div id=\"stylized\" class=\"myform\">");
		out.print("<form id=\"editDel_form\" name=\"editDel_form\" action=\"doEditDeliverable\" method=\"post\">");
		out.print("<label>Nome</label><input type=\"text\" name=\"name\" value=\"" +listaDeliverable.get(i).getName()+"\"/><br>");
		out.print("<label>Descrizione</label><input type=\"text\" name=\"description\" value=\"" +listaDeliverable.get(i).getDescription() +"\"/><br>");
		out.print("<label>Deadline</label><input type=\"text\" name=\"deadline\" value=\"" +Parser.dateFormat.format(listaDeliverable.get(i).getDeadline().getTime())+"\"/><br>");
		out.print("<input type=\"hidden\" name=\"delId\" value=\""+listaDeliverable.get(i).getId()+ "\">");
		out.print("<button type=\"submit\" value=\"Modifica\">Modifica</button>");
		out.print("<div class=\"spacer\"></div>");
		out.print("</form>");
		out.print("</div>");
		out.print("<div id=\"stylized\" class=\"myform\">");
		out.print("<form id=\"remDel_form\" name=\"remDel_form\" action=\"doRemoveDeliverable\" method=\"post\">");
		out.print("<input type=\"hidden\" name=\"delId\" value=\""+listaDeliverable.get(i).getId()+ "\">");
		out.print("<button type=\"submit\" value=\"Rimuovi\">Rimuovi</button>");
		out.print("</form>");
		out.print("</div>");
		out.print("</td>");
		out.print("</tr>");
	}
%>
</tbody>
</table>



<br>
<div id="stylized" class="myform">
<form id="newDel_form" name="newDel_form" action="doAddDeliverable" method="post">
<h1>Aggiungi Deliverable</h1>
<p>Permette di Aggiungere un Deliverable</p>
	<label>Nome<span class="small"></span></label><input type="text" name="name"/><br>
	<label>Descrizione<span class="small"></span></label><textarea name="description"></textarea><br>
	<label>Deadline<span class="small">GG-MM-AAAA HH:MM:SS</span></label><input type="text" name="deadline"/><br>
	<input type="hidden" name="prjId" value="<% if(prjDet!=null) out.print(prjDet.getId()); %>">
<button type="submit" name="action" value="Modifica">Aggiungi</button>
<div class="spacer"></div>
</form>
</div>

</body>
</html>