<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrati</title>
</head>
<body>
<%@ include file="headerGuest.jsp" %>
<center>
<div id="stylized" class="myform">
<form id="register_form" name="register_form" action="doRegister" method="post" enctype="multipart/form-data">
<h1>Registrazione</h1>
<p>Permette di Registrarsi al Sistema</p>
	<label>Username<span class="small">3-20 Caratteri</span></label><input type="text" name="username" /><br>
	<label>Password<span class="small">Pi� di 4 Caratteri</span></label><input type="password" name="password" /><br>
	<label>Nome<span class="small"></span></label><input type="text" name="firstName" /><br>
	<label>Cognome<span class="small"></span></label><input type="text" name="lastName" /><br>
	<label>Matricola<span class="small">6 Numeri</span></label><input type="text" name="matricola" /><br>
	<label>Avatar<span class="small"></span></label><input type="file" name="document" accept="image/jpg"/>
<button type="submit" name="action" value="Registrati">Registrati</button>
<div class="spacer"></div>
</form>
</div>
</center>
<%@ include file="footer.jsp" %>
</body>
</html>