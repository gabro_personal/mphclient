<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="mph.remote.*"%>
<%@page import="mph.client.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%@ include file="error.jsp" %>
<%
ProfessorManagerRemote professorManager= (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
ProfessorDetails profDetailA=null;
try{	
profDetailA=professorManager.getProfessor(mphCk, mphCk.getUserID());
} catch (Exception e) {
	request.getRequestDispatcher("login").forward(request, response);
}
%>
<table>
<tr>
<td>
<% if(profDetailA!=null) out.print(profDetailA.getFirstName());%>
<% if(profDetailA!=null) out.print(profDetailA.getLastName());%><br>
<a href="doLogout"><span>Logout</a><br>
<td>
</tr>
</table>
<ul class="menu">
 <li><a href="indexProf"><span>Index</span></a></li>
 <li><a href="manageProject"><span>Gestisci Progetti</span></a></li>
 <li><a href="evalProject"><span>Valuta Progetti</span></a></li>
</ul>
<br>
</body>
</html>