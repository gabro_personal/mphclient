<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mph.util.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="../jquery/jquery-1.7.1.js"></script>
<script language="Javascript">
function updProgetto() {
	if(document.getElementById("prjId").value=="-1"){
		document.getElementById('existPrj').style.display = "none";
		document.getElementById('newPrj').style.display = "block";
	}
	else{
		$.get("getDynamicEditProject?listType=1&prjId="+document.getElementById('prjId').value, function(response) { document.getElementById('existPrj').innerHTML=response; });
		document.getElementById('existPrj').style.display = "block";
		document.getElementById('newPrj').style.display = "none";
	}
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gestisci Progetti</title>
</head>
<body onload="updProgetto();">
<%@ include file="headerProf.jsp" %>
<%
List<ProjectDetails> prjDet=(List<ProjectDetails>)request.getAttribute( "prjList" );
%>



<div id="stylized" class="myform">
<h1>Scegli Progetto</h1>
<p></p>
	<label>Progetto<span class="small"></span></label>
	<select id="prjId" name="prjId" onChange="updProgetto();">
	<%
	for(int i=0;i<prjDet.size();i++)
		out.print("<option value='"+prjDet.get(i).getId()+"'>"+prjDet.get(i).getName()+"</option>");
	%>
	<option value='-1'>Nuovo Progetto...</option>
	</select>
<div class="spacer"></div>
</div><br>

<div id="existPrj" style="display: none"></div>
<div id="newPrj" style="display: none">
<div id="stylized" class="myform">
<form id="registerPrj_form" name="registerPrj_form" action="doRegisterProject" method="post">
<h1>Crea Progetto</h1>
<p>Permette di Creare un Nuovo Progetto</p>
	<label>Nome<span class="small"></span></label><input type="text" name="name" /><br>
	<label>Descrizione<span class="small"></span></label><textarea name="description"></textarea>
<button type="submit" name="action" value="Crea">Crea</button>
<div class="spacer"></div>
</form>
</div>
</div>
<%@ include file="footer.jsp" %>
</body>
</html>