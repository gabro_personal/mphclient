package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.UsernameTakenException;
import mph.remote.AdministratorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoRegisterProf
 */
public class DoRegisterProf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdministratorManagerRemote adminManager; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoRegisterProf() {
        super();
        this.adminManager = (AdministratorManagerRemote)GetJNDI.getByName("AdministratorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String username = request.getParameter("username") != null ? request.getParameter("username") : "";
		String password = request.getParameter("password") != null ? request.getParameter("password") : "";
		String firstName = request.getParameter("firstName") != null ? request.getParameter("firstName") : "";
		String lastName = request.getParameter("lastName") != null ? request.getParameter("lastName") : "";

		try {
			this.register(request,response,username, password, firstName,
					lastName,mphCk);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/adminIndex").forward(request, response);
	}

	private void register(HttpServletRequest request,
			HttpServletResponse response, String username,  String password,  String firstName,
			 String lastName, LoginToken mphCk) throws InvalidTokenException, InvalidFileException, UsernameTakenException, InvalidArgumentsException{
		this.adminManager.createProfessor(mphCk, username, password, firstName, lastName, null);	
		}
}
