package mph.api.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import mph.client.util.GetJNDI;
import mph.client.util.Uploader;
import mph.remote.StudentManagerRemote;
import mph.client.util.CookiesUtil;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentAlreadyEvaluatedException;
import mph.exceptions.DocumentAlreadyExistsForDeliverableException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoAddDocument
 */
public class DoAddDocument extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoAddDocument() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		List<Object> retVal=null;
		
		try {
			retVal= Uploader.getUploadData(request,response);
		} catch (FileUploadException e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/groupInfo").forward(request, response);
			return;
		}
		
		Iterator<Object> retIterator = retVal.iterator();
		@SuppressWarnings("unchecked")
		Map<String,String> formField=(HashMap<String,String>)retIterator.next();
		FileItem fileData=(FileItem)retIterator.next();
		byte[] byteFile= fileData.get();
		fileData.delete();
		
		String action = formField.get("action") != null ? formField.get("action") : "";

		try {
			if(action.equals("new")){ 
				String groupId = formField.get("groupId") != null ? formField.get("groupId") : "";
				String deliverableId = formField.get("deliverableId") != null ? formField.get("deliverableId") : "";
				this.addDoc(request,response,Integer.parseInt(deliverableId),byteFile,mphCk,Integer.parseInt(groupId));
			} else { 
				String documentId = formField.get("documentId") != null ? formField.get("documentId") : "";
				this.updateDoc(request,response,Integer.parseInt(documentId),byteFile,mphCk);
			}
			} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/groupInfo").forward(request, response);
}	
		
	private void addDoc(HttpServletRequest request,
			HttpServletResponse response,int deliverableId,byte[] byteFile,LoginToken mphCk,
			int groupId) throws ServletException, IOException, FileUploadException, InvalidTokenException, InvalidFileException, GroupNotFoundException, DeliverableNotFoundException, DocumentAlreadyExistsForDeliverableException  {
			
		this.studentManager.createDocument(mphCk, byteFile, groupId, deliverableId);
		
	}
		
	private void updateDoc(HttpServletRequest request,
			HttpServletResponse response,int documentId,byte[] byteFile,LoginToken mphCk) throws InvalidTokenException, DocumentNotFoundException, InvalidFileException, DocumentAlreadyEvaluatedException  {
		
		this.studentManager.updateDocument(mphCk, documentId, byteFile);
		
	}
	
}
