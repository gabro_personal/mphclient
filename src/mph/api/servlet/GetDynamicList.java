package mph.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.remote.StudentManagerRemote;
import mph.util.GroupDetails;
import mph.util.LoginToken;

/**
 * Servlet implementation class GetDynamicList
 */
public class GetDynamicList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDynamicList() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String prjId = request.getParameter("prjId") != null ? request
				.getParameter("prjId") : "";

				if (prjId != "") {
					int i_prjId = Integer.parseInt(prjId);
					try {
						List<GroupDetails> groupInPrj = this.studentManager
								.getGroupsForProject(mphCk,i_prjId);

						if (groupInPrj.size() == 0)
							out.print("<div onmouseover=\"nuovoGruppo();\">");

							out.print("<label>Gruppo<span class=\"small\">Nome del Gruppo</span></label>");

						out.print("<select id=\"groupId\" name=\"groupId\" onChange=\"if(this.options["
								+ groupInPrj.size() + "].selected) nuovoGruppo();\">");
						for (int i = 0; i < groupInPrj.size(); i++) {
							out.print("<option value='" + groupInPrj.get(i).getId()
									+ "'>" + groupInPrj.get(i).getName() + "</option>");
						}
						out.print("<option value='-1'>Nuovo Gruppo...</option>");
						out.print("</select>");

						if (groupInPrj.size() == 0)
							out.print("</div>");
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					out.println("Nessun Progetto/Gruppo");
				}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
