package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentAlreadyInGroupException;
import mph.exceptions.StudentInMaxOneGroupPerProjectException;
import mph.exceptions.StudentNotFoundException;
import mph.remote.StudentManagerRemote;
import mph.util.GroupDetails;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoAddGroup
 */
public class DoAddGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoAddGroup() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		

		String prjId = request.getParameter("prjId") != null ? request.getParameter("prjId") : "";
		String groupName = request.getParameter("newGroupName") != null ? request.getParameter("newGroupName") : "";
		String groupId = request.getParameter("groupId") != null ? request.getParameter("groupId") : "";
								
		
		try {
			if(!groupName.equals("")) 
				groupId=String.valueOf(this.createGroup(request,response,mphCk,groupName,Integer.parseInt(prjId)));
			this.addGroup(request,response,mphCk,Integer.parseInt(groupId));
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/groupManager").forward(request, response);
	}

	private void addGroup(HttpServletRequest request,
			HttpServletResponse response,LoginToken mphCk, int groupId) throws InvalidTokenException, StudentNotFoundException, GroupNotFoundException, GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException {
			
		this.studentManager.addStudentToGroup(mphCk,mphCk.getUserID(),groupId);
			
	}
		
	
	private int createGroup(HttpServletRequest request,
			HttpServletResponse response,LoginToken mphCk, String groupName,int prjId) throws InvalidTokenException, StudentNotFoundException, GroupNotFoundException, GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException, ProjectNotFoundException {
		
		GroupDetails newGroup = this.studentManager.createGroup(mphCk,groupName,prjId);	
		return newGroup.getId();
		
	}
}
