package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.Context;
import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.ProjectNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.remote.StudentManagerRemote;
import mph.util.GroupDetails;
import mph.util.ProfessorDetails;
import mph.util.ProjectDetails;

/**
 * Servlet implementation class PrjInfo
 */
public class PrjInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;
	ProfessorManagerRemote professorManager;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrjInfo() {
        super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Context mphContext=CookiesUtil.stdContextCookie(request,response);
		if( mphContext == null) return;
		
		try {
			this.getInfo(request,response,mphContext);
		} catch (Exception e) {
		e.printStackTrace();
		request.setAttribute("errMsg", e.getMessage());
		} 
		request.getRequestDispatcher("/api/prjInfo.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

	private void getInfo(HttpServletRequest request, HttpServletResponse response,Context mphContext) throws InvalidTokenException, GroupNotFoundException, ProjectNotFoundException, ProfessorNotFoundException {	
		
		GroupDetails groupDet = this.studentManager.getGroup(mphContext.getMphCk(),  mphContext.getMphGroup());
		int prjId = groupDet.getProjectId();
		ProjectDetails prjDet = this.studentManager.getProject(mphContext.getMphCk(), prjId);
		Integer groupNumber=this.studentManager.getGroupsForProject(mphContext.getMphCk(),prjDet.getId()).size(); 
		ProfessorDetails profName = this.professorManager.getProfessor(mphContext.getMphCk(), prjDet.getProfessorId());
		
		request.setAttribute("prjDet", prjDet);
		request.setAttribute("prjGNum", groupNumber);
		request.setAttribute("profName", profName);
			
	}
	
}
