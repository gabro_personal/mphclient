package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.remote.AccessManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoLogout
 */
public class DoLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AccessManagerRemote accessManager;

	public DoLogout() {
		super();
		this.accessManager=(AccessManagerRemote)GetJNDI.getByName("AccessManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null) return;
		
		logout(request,response,mphCk);
		
		request.getRequestDispatcher("/api/login").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	private void logout(HttpServletRequest request,
			HttpServletResponse response,LoginToken mphCk) throws ServletException, IOException{
				
		this.accessManager.logout(mphCk);
		Cookie loginCookie = new Cookie("mphToken", "");
		loginCookie.setComment("Token di Autenticazione - mphClient");
		loginCookie.setMaxAge(0);
		loginCookie.setPath("/");
		loginCookie.setSecure(false);
		response.addCookie(loginCookie);
	}
}
