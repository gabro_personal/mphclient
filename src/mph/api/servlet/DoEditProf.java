package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.remote.AdministratorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoEditProf
 */
public class DoEditProf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdministratorManagerRemote adminManager; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoEditProf() {
        super();
        this.adminManager = (AdministratorManagerRemote)GetJNDI.getByName("AdministratorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String username = request.getParameter("username") != null ? request.getParameter("username") : "";
		String password = request.getParameter("password") != null ? request.getParameter("password") : "";
		String firstName = request.getParameter("firstName") != null ? request.getParameter("firstName") : "";
		String lastName = request.getParameter("lastName") != null ? request.getParameter("lastName") : "";
		String professorId = request.getParameter("profId") != null ? request.getParameter("profId") : "";
		try {
			this.edit(request,response,username, password, firstName,
					lastName,mphCk,Integer.parseInt(professorId));
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/adminIndex").forward(request, response);
	}
	
	private void edit(HttpServletRequest request,
			HttpServletResponse response, String username,  String password,  String firstName,
			 String lastName, LoginToken mphCk,int professorId) throws InvalidTokenException, InvalidFileException {
	
		if(username.equals("")) username=null;
		if(password.equals("")) password=null;
		if(firstName.equals("")) firstName=null;
		if(lastName.equals("")) lastName=null;
		this.adminManager.updateProfessor(mphCk, professorId, username, password, firstName, lastName, null);
		
	}

}
