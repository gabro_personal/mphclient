package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.InvalidScoreException;
import mph.exceptions.InvalidTokenException;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoValuta
 */
public class DoValuta extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ProfessorManagerRemote professorManager;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoValuta() {
        super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String docId = request.getParameter("docId") != null ? request
				.getParameter("docId") : "";
		String voto = request.getParameter("voto") != null ? request
						.getParameter("voto") : "";		
				
		try {
			this.valutaDoc(request,response,mphCk,Integer.parseInt(docId),Integer.parseInt(voto));
		} catch (Exception e) {
		e.printStackTrace();
		request.setAttribute("errMsg", e.getMessage());
		} 
		request.setAttribute("selCk", "");
		request.getRequestDispatcher("/api/evalProject").include(request, response);
	}

	private void valutaDoc(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk,
			int docId, int voto) throws InvalidTokenException, DocumentNotFoundException, InvalidScoreException{
		
		this.professorManager.evaluateDocument(mphCk, docId, voto);
	}
}
