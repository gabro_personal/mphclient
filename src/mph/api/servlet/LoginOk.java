package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.GetJNDI;
import mph.exceptions.InvalidTokenException;
import mph.remote.AccessManagerRemote;
import mph.util.LoginToken;
import mph.util.UserType;

/**
 * Servlet implementation class LoginOk
 */
public class LoginOk extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AccessManagerRemote accessManager;

	public LoginOk() {
		super();
		this.accessManager=(AccessManagerRemote)GetJNDI.getByName("AccessManagerJNDI");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		UserType userType=null;
		try {
			userType = this.accessManager.getUserType((LoginToken)request.getAttribute("loginToken"));
		} catch (InvalidTokenException e) {
			e.printStackTrace();
			request.getRequestDispatcher("/api/login").include(request, response);
			return;
		}

		if(userType==UserType.PROFESSOR)
			request.setAttribute("nextPage", "indexProf");
		if(userType==UserType.STUDENT)
			request.setAttribute("nextPage", "index");
		if(userType==UserType.ADMINISTRATOR)
			request.setAttribute("nextPage", "adminIndex");
		
		request.getRequestDispatcher("/api/loginOk.jsp").include(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

}
