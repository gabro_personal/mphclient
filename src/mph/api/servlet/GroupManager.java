package mph.api.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.StudentNotFoundException;
import mph.remote.StudentManagerRemote;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;

/**
 * Servlet implementation class GroupManager
 */
public class GroupManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GroupManager() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		try {
			getAllProjects(request, response,mphCk);
			getMyGroup(request, response,mphCk);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/groupManager.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

	private void getMyGroup(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk) throws InvalidTokenException, StudentNotFoundException{
	List<GroupDetails> myGroupDet = this.studentManager.getGroupsForStudent(mphCk, mphCk.getUserID());
	request.setAttribute("myGroupList", myGroupDet);
	}
	
	private void getAllProjects(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk) throws InvalidTokenException{
	List<ProjectDetails> prjDet = this.studentManager.getAllProjects(mphCk);
	request.setAttribute("prjList", prjDet);
	}
}
