package mph.api.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;

/**
 * Servlet implementation class GetDynamicInfoByDeliv
 */
public class GetDynamicInfoByDeliv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDynamicInfoByDeliv() {
		super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		response.setContentType("text/html");
		
		String prjId = request.getParameter("prjId") != null ? request
				.getParameter("prjId") : "";
		
		try {
				int i_prjId = Integer.parseInt(prjId);	
				ProjectDetails prjDet = this.professorManager.getProject(mphCk, i_prjId);
				request.setAttribute("prjDet", prjDet);
				this.getInfo(request, response,mphCk,i_prjId);
				request.getRequestDispatcher("/api/infoByDeliv.jsp").include(request, response);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
	}

	private void getInfo(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk, int i_prjId) throws InvalidTokenException, DeliverableNotFoundException, ProjectNotFoundException, DocumentNotFoundException, GroupNotFoundException {	

		Iterator<Integer> listaDeliverableId = this.professorManager.getProject( mphCk, i_prjId).getDeliverablesIds().iterator();;
		List<DeliverableDetails> listaDeliverable=new ArrayList<DeliverableDetails>();
		Map<Integer,List<DocumentDetails>> listaDocumenti=new HashMap<Integer,List<DocumentDetails>>();
		Map<Integer,GroupDetails> listaGruppi=new HashMap<Integer,GroupDetails>();
		
		while(listaDeliverableId.hasNext()){
			Integer currId = listaDeliverableId.next(); 
			DeliverableDetails currDel = this.professorManager.getDeliverable(mphCk, currId); 
			Iterator<Integer> currDocListId = currDel.getDocumentsIds().iterator(); 
			List<DocumentDetails> currDocList=new ArrayList<DocumentDetails>();
			while(currDocListId.hasNext()){
				Integer currDocId = currDocListId.next();
				DocumentDetails currDoc = this.professorManager.getDocument(mphCk, currDocId);
				listaGruppi.put(currDocId, this.professorManager.getGroup(mphCk,currDoc.getGroupId()));
				currDocList.add(currDoc);
			}
			listaDocumenti.put(currId, currDocList);
			listaDeliverable.add(currDel);
		}
		
		request.setAttribute("mapGroup", listaGruppi);
		request.setAttribute("mapDocument", listaDocumenti);
		request.setAttribute("listaDeliverable", listaDeliverable);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
