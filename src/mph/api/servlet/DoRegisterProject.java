package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoRegisterProject
 */
public class DoRegisterProject extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	ProfessorManagerRemote professorManager;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoRegisterProject() {
        super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String description = request.getParameter("description") != null ? request
				.getParameter("description") : "";
		String name = request.getParameter("name") != null ? request
				.getParameter("name") : "";
		
				try {
					this.registerProject(request,response,name,description,mphCk);
				} catch (Exception e) {
					e.printStackTrace();
					request.setAttribute("errMsg", e.getMessage());
				}
				request.getRequestDispatcher("/api/manageProject").forward(request, response);
	}

	private void registerProject(HttpServletRequest request,HttpServletResponse response,
			String name, String description,LoginToken mphCk) throws InvalidTokenException, ProfessorNotFoundException  {
		this.professorManager.createProject(mphCk, name, description, mphCk.getUserID());
	}
}
