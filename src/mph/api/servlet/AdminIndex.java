package mph.api.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.remote.AdministratorManagerRemote;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;

/**
 * Servlet implementation class AdminIndex
 */
public class AdminIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdministratorManagerRemote adminManager; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminIndex() {
        super();
        this.adminManager = (AdministratorManagerRemote)GetJNDI.getByName("AdministratorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;		
				
		try {
			List<ProfessorDetails> profList=this.adminManager.getAllProfessors(mphCk);
			request.setAttribute("profList", profList);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/adminIndex.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
