package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.GetJNDI;
import mph.client.util.MagicConst;
import mph.exceptions.LoginException;
import mph.remote.AccessManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoLogin
 */
public class DoLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AccessManagerRemote accessManager;

	public DoLogin() {
		super();
		this.accessManager=(AccessManagerRemote)GetJNDI.getByName("AccessManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("username") != null ? request
				.getParameter("username") : "";
		String password = request.getParameter("password") != null ? request
				.getParameter("password") : "";
				
		try {
			this.login(request,response,username,password);
			request.getRequestDispatcher("/api/loginOk").forward(request, response);
		} catch (LoginException e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/login").forward(request, response);
		}	
		
	}

	private void login(HttpServletRequest request,
			HttpServletResponse response, String username,String password) throws LoginException {
		
		LoginToken loginTk = this.accessManager.login(username,password);
		Cookie loginCookie = new Cookie("mphToken", loginTk.toString());
		loginCookie.setComment("Token di Autenticazione - mphClient");
		loginCookie.setMaxAge(MagicConst.TTL);
		loginCookie.setPath("/");
		loginCookie.setSecure(false);
		response.addCookie(loginCookie);
		request.setAttribute("loginToken", loginTk);
		
	}
	
}
