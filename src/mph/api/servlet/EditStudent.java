package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.remote.StudentManagerRemote;
import mph.util.LoginToken;
import mph.util.StudentDetails;

/**
 * Servlet implementation class EditStudent
 */
public class EditStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager; 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditStudent() {
        super();
        this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;		
				
		try {
			StudentDetails studDetail = this.studentManager.getStudent(mphCk, mphCk.getUserID());
			request.setAttribute("studDetail", studDetail);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/editStudent.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

}
