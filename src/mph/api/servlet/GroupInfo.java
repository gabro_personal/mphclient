package mph.api.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.Context;
import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentNotFoundException;
import mph.remote.StudentManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.ProjectDetails;
import mph.util.StudentDetails;

/**
 * Servlet implementation class GroupInfo
 */
public class GroupInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager; 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupInfo() {
        super();
        this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Context mphContext=CookiesUtil.stdContextCookie(request,response);
		if( mphContext == null) return;
		
		try {
			this.getInfo(request,response,mphContext);
		} catch (Exception e) {
		e.printStackTrace();
		request.setAttribute("errMsg", e.getMessage());
		} 
		request.getRequestDispatcher("/api/groupInfo.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

	private void getInfo(HttpServletRequest request, HttpServletResponse response,Context mphContext) throws InvalidTokenException, GroupNotFoundException, ProjectNotFoundException, DocumentNotFoundException, StudentNotFoundException {	
		
		GroupDetails groupDet = this.studentManager.getGroup(mphContext.getMphCk(),  mphContext.getMphGroup());
		List<StudentDetails> listaComponenti = this.studentManager.getStudentsForGroup(mphContext.getMphCk(), groupDet.getId());
		ProjectDetails progettoAssegnato = this.studentManager.getProject(mphContext.getMphCk(), groupDet.getProjectId());
		List<DeliverableDetails> listaDeliverable = this.studentManager.getDeliverablesForProject(mphContext.getMphCk(), groupDet.getProjectId());
		HashMap<Integer,DocumentDetails> docMap = new HashMap<Integer,DocumentDetails>();
		List<Integer> docId = groupDet.getDocumentsIds();

		for(int i=0;i<docId.size();i++){
			DocumentDetails localDoc = this.studentManager.getDocument(mphContext.getMphCk(),docId.get(i));
			if(localDoc!=null)
			docMap.put(
					localDoc.getDeliverableId(),
					localDoc
					);
		}
		
		request.setAttribute("listaComponenti",  listaComponenti);
		request.setAttribute("progettoAssegnato", progettoAssegnato);
		request.setAttribute("listaDeliverable", listaDeliverable);
		request.setAttribute("groupDet", groupDet);	
		request.setAttribute("docMap", docMap);	
	}
}
