package mph.api.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;
import mph.util.ProjectDetails;

/**
 * Servlet implementation class EvalProject
 */
public class EvalProject extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EvalProject() {
		super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		try {
			getAllProjects(request, response,mphCk);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/evalProject.jsp").include(request, response);
	}
	
	private void getAllProjects(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk) throws InvalidTokenException, ProfessorNotFoundException{
	List<ProjectDetails> prjDet = this.professorManager.getProjectsForProfessor(mphCk, mphCk.getUserID());
	request.setAttribute("prjList", prjDet);
	}
}
