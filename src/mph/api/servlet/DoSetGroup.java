package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.MagicConst;

/**
 * Servlet implementation class DoSetGroup
 */
public class DoSetGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoSetGroup() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String setGroupId = request.getParameter("setGroupId") != null ? request
				.getParameter("setGroupId") : "";
		String actionRedir= request.getParameter("action") != null ? request
						.getParameter("action") : "";
		
		if(actionRedir.equals("Rimuovi da Gruppo"))	{
			request.getRequestDispatcher("/api/doRemoveFromGroup").forward(request, response);
			return;
		}
		this.setGroup(request,response,setGroupId);
		request.getRequestDispatcher("/api/groupManager").forward(request, response);
	}

	private void setGroup(HttpServletRequest request, HttpServletResponse response,String setGroupId) {

		Cookie groupCookie = new Cookie("setGroupId", setGroupId);
		groupCookie.setComment("Token Id Gruppo Attuale - mphClient");
		groupCookie.setMaxAge(MagicConst.TTL);
		groupCookie.setSecure(false);
		groupCookie.setPath("/");
		response.addCookie(groupCookie);
		
	}
}
