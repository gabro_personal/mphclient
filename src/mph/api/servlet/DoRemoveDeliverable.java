package mph.api.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoRemoveDeliverable
 */
public class DoRemoveDeliverable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager;  
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoRemoveDeliverable() {
        super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String delId = request.getParameter("delId") != null ? request
								.getParameter("delId") : "";
				try {
					this.removeDeliverable(request,response,mphCk,Integer.parseInt(delId));
				} catch (Exception e) {
					e.printStackTrace();
					request.setAttribute("errMsg", e.getMessage());
				}
			
		request.getRequestDispatcher("/api/manageProject").forward(request, response);
	}
	
	private void removeDeliverable(HttpServletRequest request,HttpServletResponse response,
			LoginToken mphCk,int delId) throws InvalidTokenException, DeliverableNotFoundException {
		this.professorManager.deleteDeliverable(mphCk, delId);
	}
}
