package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;

/**
 * Servlet implementation class Index
 */
public class IndexProf extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ProfessorManagerRemote professorManager; 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexProf() {
        super();
        this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;		
				
		try {
			ProfessorDetails profDetail=this.professorManager.getProfessor(mphCk, mphCk.getUserID());
			request.setAttribute("profDetail", profDetail);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/indexProf.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}
}
