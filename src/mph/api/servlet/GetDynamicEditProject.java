package mph.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;


/**
 * Servlet implementation class GetDynamicEditProject
 */
public class GetDynamicEditProject extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDynamicEditProject() {
		super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String prjId = request.getParameter("prjId") != null ? request
				.getParameter("prjId") : "";

				try {
					int i_prjId = Integer.parseInt(prjId);	
					ProjectDetails prjDet = this.professorManager.getProject(mphCk, i_prjId);
					request.setAttribute("prjDet", prjDet);
					this.getInfo(request, response,mphCk,i_prjId);
					request.getRequestDispatcher("/api/editProject.jsp").include(request, response);
					} catch (Exception e) {
						e.printStackTrace();
						out.println("Nessun Progetto");
					}
	}

private void getInfo(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk, int i_prjId) throws InvalidTokenException, ProjectNotFoundException, DeliverableNotFoundException{	

	Iterator<Integer> listaDeliverableId = this.professorManager.getProject( mphCk, i_prjId).getDeliverablesIds().iterator();;
	List<DeliverableDetails> listaDeliverable=new ArrayList<DeliverableDetails>();
	
	while(listaDeliverableId.hasNext())
		listaDeliverable.add(this.professorManager.getDeliverable(mphCk, listaDeliverableId.next()));
	request.setAttribute("listaDeliverable", listaDeliverable);
	
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
