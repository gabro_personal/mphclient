package mph.api.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import mph.client.util.GetJNDI;
import mph.client.util.Uploader;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.UsernameTakenException;
import mph.remote.StudentManagerRemote;

/**
 * Servlet implementation class DoRegister
 */
public class DoRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoRegister() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Object> retVal=null;

		try {
			retVal= Uploader.getUploadData(request,response);
		} catch (FileUploadException e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/register").forward(request, response);
			return;
		}
		
		Iterator<Object> retIterator = retVal.iterator();
		@SuppressWarnings("unchecked")
		Map<String,String> formField=(HashMap<String,String>)retIterator.next();
		FileItem fileData=(FileItem)retIterator.next();
		byte[] byteFile= fileData.get();
		fileData.delete();
		
		String username = formField.get("username") != null ? formField.get("username") : "";
		String password = formField.get("password") != null ? formField.get("password") : "";
		String firstName = formField.get("firstName") != null ? formField.get("firstName") : "";
		String lastName = formField.get("lastName") != null ? formField.get("lastName") : "";
		String matricola = formField.get("matricola") != null ? formField.get("matricola") : "";
				

		try {
			this.register(request,response,username, password, firstName,
							lastName, matricola,byteFile);
			request.getRequestDispatcher("/api/login").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/register").forward(request, response);
		}

	}
	
	private void register(HttpServletRequest request,
			HttpServletResponse response, String username,  String password,  String firstName,
			 String lastName, String matricola,byte[] byteFile) throws UsernameTakenException, InvalidArgumentsException, InvalidFileException {
	
		this.studentManager.createStudent(username, password, firstName,
				lastName, matricola,byteFile);
	}

}








