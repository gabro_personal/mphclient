package mph.api.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.StudentNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.remote.StudentManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.LoginToken;

/**
 * Servlet implementation class GetFile
 */
public class GetFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;
	ProfessorManagerRemote professorManager;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetFile() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String loadType = request.getParameter("type") != null ? request.getParameter("type") : "";
		String idFile = request.getParameter("id") != null ? request.getParameter("id") : "";
		
		try {
		
			if(loadType.equals("img"))
				loadAvatar(request, response,mphCk,Integer.parseInt(idFile));
			if(loadType.equals("pdf"))
				loadDocument(request, response,mphCk,Integer.parseInt(idFile));
			if(loadType.equals("zip"))
				loadZip(request, response,mphCk,Integer.parseInt(idFile));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

	private void loadAvatar(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk
			,int idFile) throws InvalidTokenException, StudentNotFoundException, IOException, ServletException{
		
		response.setContentType("image/jpg");
		 OutputStream outputFile = response.getOutputStream();
		byte[] imgData = this.studentManager.getStudent(mphCk, idFile).getAvatar();
		
		if(imgData.length==0)
			 request.getRequestDispatcher("/defaultImg/avatarStudent.jpg").include(request, response);
		 else
	       outputFile.write(imgData);
		
	       outputFile.flush(); 
	       outputFile.close();
	}
	
	private void loadDocument(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk
			,int idFile) throws InvalidTokenException, DocumentNotFoundException, IOException, GroupNotFoundException, DeliverableNotFoundException{
		
		response.setContentType("application/pdf");
		DocumentDetails currDoc = this.studentManager.getDocument(mphCk, idFile);
		GroupDetails currGroup = this.studentManager.getGroup(mphCk, currDoc.getGroupId());
		DeliverableDetails currDeliv = this.studentManager.getDeliverable(mphCk, currDoc.getDeliverableId());
		String fileName=currGroup.getName()+currDeliv.getName();
		response.setHeader("Content-Disposition","inline; filename="+fileName+".pdf;");  
	    OutputStream outputFile = response.getOutputStream();
		byte[] pdfData = currDoc.getFileData();
	       outputFile.write(pdfData);
	       outputFile.flush(); 
	       outputFile.close();
	}
	
	private void loadZip(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk
			,int idFile) throws InvalidTokenException, DocumentNotFoundException, IOException, GroupNotFoundException, DeliverableNotFoundException{
		
		String fileName="";
		GroupDetails currGroup = null;
		DeliverableDetails currDeliv = null;
		String subType = request.getParameter("subType") != null ? request.getParameter("subType") : "";
		
		if(subType.equals("byGroup")){
			currGroup = this.professorManager.getGroup(mphCk, idFile);
			fileName=currGroup.getName()+"Doc";
		}else{
			currDeliv = this.professorManager.getDeliverable(mphCk, idFile);
			fileName=currDeliv.getName()+"Doc";
		}
		
		response.setContentType("application/zip");
		response.setHeader("Content-Disposition","inline; filename="+fileName+".zip;");  
		
	    OutputStream outputFile = response.getOutputStream();
	    ZipOutputStream zipFile = new ZipOutputStream(outputFile); 

		if(subType.equals("byGroup")){
			Iterator<Integer> docList=currGroup.getDocumentsIds().iterator();
			while(docList.hasNext()){
				DocumentDetails currDoc = this.professorManager.getDocument(mphCk, docList.next());
				String currFileName=this.professorManager.getDeliverable(mphCk, currDoc.getDeliverableId()).getName();
				 zipFile.putNextEntry(new ZipEntry(currFileName+".pdf")); 	    	    
				byte[] pdfData = currDoc.getFileData();
				zipFile.write(pdfData);
			}
		}
		else{
			Iterator<Integer> docList=currDeliv.getDocumentsIds().iterator();
			while(docList.hasNext()){
				DocumentDetails currDoc = this.professorManager.getDocument(mphCk, docList.next());
				String currFileName=this.professorManager.getGroup(mphCk, currDoc.getGroupId()).getName();
			    zipFile.putNextEntry(new ZipEntry(currFileName+".pdf")); 	    
				byte[] pdfData = currDoc.getFileData();
				zipFile.write(pdfData);
			}
		}
	
		zipFile.flush(); 
		zipFile.close(); 
	    outputFile.flush(); 
	    outputFile.close();
	}
}
