package mph.api.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;

/**
 * Servlet implementation class GetDynamicInfoByGroup
 */
public class GetDynamicInfoByGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDynamicInfoByGroup() {
		super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		response.setContentType("text/html");
		
		String prjId = request.getParameter("prjId") != null ? request
				.getParameter("prjId") : "";
				
				try {
					int i_prjId = Integer.parseInt(prjId);	
					ProjectDetails prjDet = this.professorManager.getProject(mphCk, i_prjId);
					request.setAttribute("prjDet", prjDet);
					this.getInfo(request, response,mphCk,i_prjId);
					request.getRequestDispatcher("/api/infoByGroup.jsp").include(request, response);
						} catch (Exception e) {
							e.printStackTrace();
						}
	}

	private void getInfo(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk, int i_prjId) throws InvalidTokenException, GroupNotFoundException, ProjectNotFoundException, DocumentNotFoundException, DeliverableNotFoundException {	
		
		Iterator<Integer> listGroupId=this.professorManager.getProject( mphCk, i_prjId).getGroupsIds().iterator();
		List<GroupDetails> listGroup=new ArrayList<GroupDetails>();
		Map<Integer,List<DocumentDetails>> listaDocumenti=new HashMap<Integer,List<DocumentDetails>>();
		Map<Integer,DeliverableDetails> listaDeliverable=new HashMap<Integer,DeliverableDetails>();
		
		while(listGroupId.hasNext()){
			Integer currGroupId = listGroupId.next(); 
			GroupDetails currGroup = this.professorManager.getGroup(mphCk, currGroupId);
			Iterator<Integer> currDocListId = currGroup.getDocumentsIds().iterator(); 
			List<DocumentDetails> currDocList=new ArrayList<DocumentDetails>();
			while(currDocListId.hasNext()){
				Integer currDocId = currDocListId.next();
				DocumentDetails currDoc = this.professorManager.getDocument(mphCk, currDocId);
				currDocList.add(currDoc);
				listaDeliverable.put(currDocId, this.professorManager.getDeliverable(mphCk, currDoc.getDeliverableId()));
			}
			
			listGroup.add(currGroup);
			listaDocumenti.put(currGroupId, currDocList);
		}
		request.setAttribute("mapDeliverable", listaDeliverable);
		request.setAttribute("listGroup", listGroup);
		request.setAttribute("mapDocument", listaDocumenti);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
