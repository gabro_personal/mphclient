package mph.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;

import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.StudentNotInGroupException;
import mph.remote.StudentManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoRemoveFromGroup
 */
public class DoRemoveFromGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StudentManagerRemote studentManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoRemoveFromGroup() {
		super();
		this.studentManager = (StudentManagerRemote)GetJNDI.getByName("StudentManagerJNDI");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String setGroupId = request.getParameter("setGroupId") != null ? request
				.getParameter("setGroupId") : "";
				
		try {
			this.removeFrom(request,response,Integer.parseInt(setGroupId));
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
		}
		request.getRequestDispatcher("/api/groupManager").forward(request, response);
	}

	private void removeFrom(HttpServletRequest request, HttpServletResponse response,int removeGroupId) throws ServletException, IOException, InvalidTokenException, StudentNotFoundException, GroupNotFoundException, GroupEmptyException, StudentNotInGroupException {
		LoginToken mphCk=CookiesUtil.stdLoginCookie(request,response);
		if( mphCk == null) return;
		this.studentManager.removeStudentFromGroup(mphCk, mphCk.getUserID(), removeGroupId);
		Cookie groupCookie = new Cookie("mphGId", "");
		groupCookie.setComment("Token Id Gruppo Attuale - mphClient");
		groupCookie.setMaxAge(0);
		groupCookie.setSecure(false);
		groupCookie.setPath("/");
		response.addCookie(groupCookie);
		
	}
}
