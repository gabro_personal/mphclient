package mph.api.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentNotFoundException;
import mph.remote.ProfessorManagerRemote;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;
import mph.util.StudentDetails;

/**
 * Servlet implementation class GroupInfo
 */
public class GroupInfoProf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ProfessorManagerRemote professorManager; 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupInfoProf() {
        super();
        this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String strGroupId = request.getParameter("id") != null ? request
				.getParameter("id") : "";
				
		try {
			this.getInfo(request,response,mphCk,Integer.parseInt(strGroupId));
		} catch (Exception e) {
		e.printStackTrace();
		request.setAttribute("errMsg", e.getMessage());
		} 
		request.getRequestDispatcher("/api/groupInfoProf.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}

	private void getInfo(HttpServletRequest request, HttpServletResponse response,LoginToken mphCk
			,int groupId) throws InvalidTokenException, GroupNotFoundException, ProjectNotFoundException, StudentNotFoundException {	
		
		GroupDetails groupDet = this.professorManager.getGroup(mphCk, groupId);
		List<StudentDetails> listaComponenti =new ArrayList<StudentDetails>();
		ProjectDetails progettoAssegnato=this.professorManager.getProject(mphCk, groupDet.getProjectId());
		
		Iterator<Integer> studIdList = groupDet.getMembersIds().iterator();
		while(studIdList.hasNext())
			listaComponenti.add(this.professorManager.getStudent(mphCk,studIdList.next()));
			
		request.setAttribute("listaComponenti",  listaComponenti);
		request.setAttribute("progettoAssegnato", progettoAssegnato);
		request.setAttribute("groupDet", groupDet);	
	}
}
