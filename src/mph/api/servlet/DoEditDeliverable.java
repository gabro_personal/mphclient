package mph.api.servlet;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.util.CookiesUtil;
import mph.client.util.GetJNDI;
import mph.client.util.Parser;
import mph.exceptions.DeliverableNameExistsException;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.remote.ProfessorManagerRemote;
import mph.util.LoginToken;

/**
 * Servlet implementation class DoEditDeliverable
 */
public class DoEditDeliverable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	ProfessorManagerRemote professorManager;  
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoEditDeliverable() {
        super();
		this.professorManager = (ProfessorManagerRemote)GetJNDI.getByName("ProfessorManagerJNDI");
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginToken mphCk = CookiesUtil.stdLoginCookie(request,response);
		if(mphCk==null)return;
		
		String description = request.getParameter("description") != null ? request
				.getParameter("description") : "";
		String name = request.getParameter("name") != null ? request
				.getParameter("name") : "";
		String deadline = request.getParameter("deadline") != null ? request
						.getParameter("deadline") : "";
		String delId = request.getParameter("delId") != null ? request
								.getParameter("delId") : "";
				try {
					this.editDeliverable(request,response,name,description,deadline,mphCk,Integer.parseInt(delId));
				} catch (Exception e) {
					e.printStackTrace();
					request.setAttribute("errMsg", e.getMessage());
				}
		request.getRequestDispatcher("/api/manageProject").forward(request, response);
		
	}

	private void editDeliverable(HttpServletRequest request,HttpServletResponse response,
			String name, String description,String deadline,LoginToken mphCk,int delId) throws InvalidTokenException, DeliverableNotFoundException, DeliverableNameExistsException, ParseException  {
		this.professorManager.updateDeliverable(mphCk, delId, name, description, Parser.parseCalendarData(deadline));

	}

}
