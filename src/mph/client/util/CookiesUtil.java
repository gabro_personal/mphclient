package mph.client.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mph.client.exceptions.CookieNotFoundException;
import mph.util.LoginToken;

public class CookiesUtil {
	
public static Cookie getLoginCookie(Cookie[] cookiesUtente,String cookieName) throws CookieNotFoundException{
	 if(cookiesUtente!=null)
	 for(int index=0; index<cookiesUtente.length;index++){
		 if (cookiesUtente[index].getName().equals(cookieName)) {
			 return cookiesUtente[index];
		 }
	 }
	 throw new CookieNotFoundException();
 }

public static int stdInfoCookie(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		String mphGroup;
		try {
			mphGroup = CookiesUtil
					.getLoginCookie(request.getCookies(), "setGroupId")
					.getValue();
			try {
				return Integer.parseInt(mphGroup);
			}
			catch(NumberFormatException e) {
				throw new CookieNotFoundException();
			}
		} catch (CookieNotFoundException e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/groupManager").forward(request, response);
		}
		return -1;	
}

public static LoginToken stdLoginCookie(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		String mphCk;
		try {
			mphCk = CookiesUtil
					.getLoginCookie(request.getCookies(), "mphToken")
					.getValue();
		    return new LoginToken(mphCk);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errMsg", e.getMessage());
			request.getRequestDispatcher("/api/login").forward(request, response);
		}
		return null;
}

public static Context stdContextCookie(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException{	
	LoginToken mphCk=stdLoginCookie(request,response);
	if(mphCk==null) return null;
	int mphGroup=stdInfoCookie(request,response);
	if(mphGroup==-1) return null;
	return new Context(mphCk,mphGroup);
}

public static boolean isLogin(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		try {
			CookiesUtil
					.getLoginCookie(request.getCookies(), "mphToken")
					.getValue();
		    return true;
		} catch (Exception e) {
			return false;
		}
}

}

