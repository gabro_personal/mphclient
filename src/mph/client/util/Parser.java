package mph.client.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Parser {

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public static Calendar parseCalendarData(String strFormat) throws ParseException {
		Calendar calFormat = Calendar.getInstance();
		Date dateVal = (Date)dateFormat.parse(strFormat); 
		calFormat.setTime(dateVal);
		return calFormat;
	}
}
