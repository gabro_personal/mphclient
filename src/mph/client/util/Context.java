package mph.client.util;

import mph.util.LoginToken;

public class Context {

	LoginToken mphCk;
	int mphGroup;
	
	public Context(LoginToken mphCk,int mphGroup) {
		this.mphCk=mphCk;
		this.mphGroup=mphGroup;
	}

	public LoginToken getMphCk() {
		return mphCk;
	}

	public void setMphCk(LoginToken mphCk) {
		this.mphCk = mphCk;
	}

	public int getMphGroup() {
		return mphGroup;
	}

	public void setMphGroup(int mphGroup) {
		this.mphGroup = mphGroup;
	}
	

}
