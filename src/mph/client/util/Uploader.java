package mph.client.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class Uploader {

	public static List<Object> getUploadData(HttpServletRequest request,
			HttpServletResponse response) throws FileUploadException, IOException{
	
	boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
	if (!isMultipartContent) {
		throw new FileUploadException();
	}

	FileItemFactory dataFactory = new DiskFileItemFactory();
	ServletFileUpload uploadServlet = new ServletFileUpload(dataFactory);
		@SuppressWarnings("unchecked")
		List<FileItem> requestFields = (List<FileItem>)uploadServlet.parseRequest(request);
		Iterator<FileItem> fieldsIterator = requestFields.iterator();
		Map<String,String> formField=new HashMap<String,String>();
		List<Object> returnVal=new ArrayList<Object>();
		FileItem returnFileItem=null;
		
		if (!fieldsIterator.hasNext()) {
			throw new FileUploadException();
		}
		
		while (fieldsIterator.hasNext()) {
			FileItem fileItem = fieldsIterator.next();
			boolean isFormField = fileItem.isFormField();
			if (isFormField) {
				formField.put(fileItem.getFieldName(), fileItem.getString());
			} else {
				returnFileItem=fileItem;
			}
		}
		if (returnFileItem==null) throw new FileUploadException();
		returnVal.add(formField);
		returnVal.add(returnFileItem);
		return returnVal;
}
}
