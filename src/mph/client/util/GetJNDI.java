package mph.client.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class GetJNDI {

	public static Object getByName(String nameJNDI){
		try {
			Context jndiContext = new InitialContext();
			Object ref = jndiContext.lookup(nameJNDI);
			return ref;
		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
