package mph.client.exceptions;

public class CookieNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public CookieNotFoundException() {
		super("Cookie Non Trovato");
	}
}
